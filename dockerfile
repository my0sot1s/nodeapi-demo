FROM node:8.16.0-alpine
RUN mkdir -p /app
WORKDIR app/
COPY . .

RUN npm install --production
EXPOSE 3000

CMD ["npm", "start"]